import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import xlrd
#def huber_loss(labels, predictions, delta=1.0):
#	residual = tf.abs(predictions - labels)
#	condition = tf.less(residual, delta)
#	small_res = 0.5 * tf.square(residual)
#	large_res = delta * residual - 0.5 * tf.square(delta)
#	return tf.select(condition, small_res, large_res)
DATA_FILE = "fire_theft.xls"
# Step 1: read in data from the .xls file
book = xlrd.open_workbook(DATA_FILE, encoding_override="utf-8")
sheet = book.sheet_by_index(0)
data = np.asarray([sheet.row_values(i) for i in range(1, sheet.nrows)])
n_samples = sheet.nrows - 1
# Step 2: create placeholders for input X (number of fire) and label Y (number of theft)
X = tf.placeholder(tf.float32, name="X")
Y = tf.placeholder(tf.float32, name="Y")
# Step 3: create weight and bias, initialized to 0
w = tf.Variable(0.0, name="weights_1")
u = tf.Variable(0.0, name="weights_2")
b = tf.Variable(0.0, name="bias")
# Step 4: construct model to predict Y (number of theft) from the number of fire
Y_predicted = X*X*w+X * u + b
# Step 5: use the square error as the loss function
loss = tf.square(Y - Y_predicted, name="loss")
#loss = huber_loss(Y,Y_predicted)
# Step 6: using gradient descent with learning rate of 0.01 to minimize loss
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.0000001).minimize(loss)
listx=[]
listy=[]
with tf.Session() as sess:
# Step 7: initialize the necessary variables, in this case, w and b
	writer = tf.summary.FileWriter('./graph',sess.graph)
	sess.run(tf.global_variables_initializer())
# Step 8: train the model
	for x, y in data:
		listx+=[x]
		listy+=[y]
	for i in range(100): # run 100 epochs
		for x, y in data:
# Session runs train_op to minimize loss
			sess.run(optimizer, feed_dict={X: x, Y:y})
# Step 9: output the values of w and b
	w_value,u_value, b_value = sess.run([w,u, b])
	print("W:",w_value)
	print("b:",b_value)
plt.plot(listx,listy,"ro")
p=np.arange(1,50,1)
q=w_value*p*p+u_value*p+b_value
plt.plot(p,q)
plt.show()
writer.close()