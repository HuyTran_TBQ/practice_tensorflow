import time
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
# Step 1: Read in data
# using TF Learn's built in function to load MNIST data to the folder data/mnist
MNIST = input_data.read_data_sets("/data/mnist", one_hot=True)
# Step 2: Define parameters for the model
learning_rate = 0.001
batch_size = 500
n_epochs = 10000

X = tf.placeholder(tf.float32, [batch_size, 28*28])
Y = tf.placeholder(tf.float32, [batch_size, 10])

w = tf.Variable(tf.random_normal(shape=[28*28, 300], stddev=0.01), name="weights")
w2 = tf.Variable(tf.random_normal(shape=[300, 300], stddev=0.01), name="weights_2")
w3 = tf.Variable(tf.random_normal(shape=[300, 10], stddev=0.01), name="weights_3")
b = tf.Variable(tf.zeros([1, 300]), name="bias")
b2 = tf.Variable(tf.zeros([1, 300]), name="bias_2")
b3 = tf.Variable(tf.zeros([1, 10]), name="bias_3")

logits = tf.matmul(X, w) + b
logits_2 = tf.matmul(logits, w2) + b2
logits_3 = tf.matmul(logits_2, w3) + b3

entropy = tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=logits_3)

loss = tf.reduce_mean(entropy) # computes the mean over examples in the batch

optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss)

init = tf.global_variables_initializer()
sess = tf.Session()
saver = tf.train.Saver()
sess.run(init)
n_batches = int(MNIST.train.num_examples/batch_size)
i=10
for i in range(n_epochs): # train the model n_epochs times
	for _ in range(n_batches):
		X_batch, Y_batch = MNIST.train.next_batch(batch_size)
		_,los = sess.run([optimizer, loss], feed_dict={X: X_batch, Y:Y_batch})
	if i%10==0:
		print(i)
		i=i+10
		print(los)

print(los)
# average loss should be around 0.35 after 25 epochs

n_batches = int(MNIST.test.num_examples/batch_size)
total_correct_preds = 0

save_path = saver.save(sess, "/home/ubuntu/tensor/model1")
print("Model saved in file: %s" % save_path)

#saver.restore(sess, "/tmp/model.ckpt")
#print("Model restored.")

for i in range(n_batches):
	X_batch, Y_batch = MNIST.test.next_batch(batch_size)
	logits_batch = sess.run(logits_3, feed_dict={X: X_batch, Y:Y_batch})
	preds = tf.nn.softmax(logits_batch)
	correct_preds = tf.equal(tf.argmax(preds, 1), tf.argmax(Y_batch, 1))
	accuracy = tf.reduce_sum(tf.cast(correct_preds, tf.float32)) # similar to numpy.count_nonzero(boolarray) :(
	total_correct_preds += sess.run(accuracy)
print("Accuracy",format(total_correct_preds/MNIST.test.num_examples))
