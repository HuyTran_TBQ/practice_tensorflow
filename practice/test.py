import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import xlrd
def huber_loss(labels, predictions, delta=1.0):
	residual = tf.abs(predictions - labels)
	condition = tf.less(residual, delta)
	small_res = 0.5 * tf.square(residual)
	large_res = delta * residual - 0.5 * tf.square(delta)
	return tf.select(condition, small_res, large_res)

X_in = np.linspace(-1,1,100)
Y_in = 3*X_in + np.random.randn(X_in.shape[0])*0.5
X = tf.placeholder(tf.float32, name="X")
Y = tf.placeholder(tf.float32, name="Y")
# Step 3: create weight and bias, initialized to 0
w = tf.Variable(0.0, name="weights")
b = tf.Variable(0.0, name="bias")
# Step 4: construct model to predict Y (number of theft) from the number of fire
Y_predicted = X * w + b
# Step 5: use the square error as the loss function
loss = tf.square(Y - Y_predicted, name="loss")
#loss = huber_loss(Y,Y_predicted)
# Step 6: using gradient descent with learning rate of 0.01 to minimize loss
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(loss)
with tf.Session() as sess:
# Step 7: initialize the necessary variables, in this case, w and b
	sess.run(tf.global_variables_initializer())
# Step 8: train the model
	for i in range(50): # run 100 epochs
# Session runs train_op to minimize loss
		sess.run(optimizer, feed_dict={X: X_in, Y:Y_in})
# Step 9: output the values of w and b
	w_value, b_value = sess.run([w, b])
	print("W:",w_value)
	print("b:",b_value)
plt.plot(X_in,Y_in,"ro")
p=np.arange(-1,1,0.1)
q=w_value * p + b_value
plt.plot(p,q)
plt.show()