import time
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
# Step 1: Read in data
# using TF Learn's built in function to load MNIST data to the folder data/mnist
MNIST = input_data.read_data_sets("/data/mnist", one_hot=True)
# Step 2: Define parameters for the model
learning_rate = 0.001
batch_size = 400
n_epochs = 1000

X = tf.placeholder(tf.float32, [batch_size, 28*28])
Y = tf.placeholder(tf.float32, [batch_size, 10])

x_image = tf.reshape(X, [-1,28,28,1])

n_batches = int(MNIST.train.num_examples/batch_size)

W_conv1 = tf.Variable(tf.random_normal(shape=[5, 5, 1, 32], stddev=0.01))
b_conv1 = tf.Variable(tf.zeros([32]))
h_conv1 = tf.nn.relu(tf.nn.conv2d(x_image, W_conv1, strides=[1, 1, 1, 1], padding='SAME') + b_conv1)
h_pool1 = tf.nn.max_pool(h_conv1, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')
						
W_conv2 = tf.Variable(tf.random_normal(shape=[5, 5, 32, 64], stddev=0.01))
b_conv2 = tf.Variable(tf.zeros([64]))
h_conv2 = tf.nn.relu(tf.nn.conv2d(h_pool1, W_conv2, strides=[1, 1, 1, 1], padding='SAME') + b_conv2)
h_pool2 = tf.nn.max_pool(h_conv2, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')
						

						
W_fc1 = tf.Variable(tf.random_normal(shape=[7 * 7 * 64, 1024], stddev=0.01))
b_fc1 = tf.Variable(tf.zeros([1024]))
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

W_fc2 = tf.Variable(tf.random_normal(shape=[1024, 10], stddev=0.01))
b_fc2 = tf.Variable(tf.zeros([10]))
y_conv = tf.matmul(h_fc1, W_fc2) + b_fc2

entropy = tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=y_conv)
loss = tf.reduce_mean(entropy) # computes the mean over examples in the batch
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss)

init = tf.global_variables_initializer()
sess = tf.InteractiveSession()
saver = tf.train.Saver()
sess.run(init)
count=0
for i in range(n_epochs): # train the model n_epochs times
	for _ in range(n_batches):
		X_batch, Y_batch = MNIST.train.next_batch(batch_size)
		_,los = sess.run([optimizer, loss], feed_dict={X: X_batch, Y:Y_batch})
	print(los)
	count=count+1
	print(count)

n_batches = int(MNIST.test.num_examples/batch_size)
total_correct_preds = 0

save_path = saver.save(sess, "/home/ubuntu/tensor/model2/model2")
print("Model saved in file: %s" % save_path)
for i in range(n_batches):
	X_batch, Y_batch = MNIST.test.next_batch(batch_size)
	y_predict = sess.run(y_conv, feed_dict={X: X_batch, Y:Y_batch})
	preds = tf.nn.softmax(y_predict)
	correct_preds = tf.equal(tf.argmax(preds, 1), tf.argmax(Y_batch, 1))
	accuracy = tf.reduce_sum(tf.cast(correct_preds, tf.float32)) # similar to numpy.count_nonzero(boolarray) :(
	total_correct_preds += sess.run(accuracy)
print("Accuracy",format(total_correct_preds/MNIST.test.num_examples))

